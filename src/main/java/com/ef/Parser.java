package com.ef;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.ef.service.LogService;

@SpringBootApplication
public class Parser {

	private static String startDate = "2017-01-01.00:00:00";
	private static String duration = "hourly";
	private static String threshold = "0";
	private static String filename = "access.log";
	private static String upload = "Y";

	@Autowired
	private LogService logService;
	private static LogService service;

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(Parser.class);
		app.run(args);

		for (String arg : args) {

			String[] temp = arg.split("=");

			if (arg.contains("startDate")) {
				startDate = temp[1];
			} else if (arg.contains("duration")) {
				duration = temp[1];
			} else if (arg.contains("threshold")) {
				threshold = temp[1];
			} else if (arg.contains("upload")) {
				upload = temp[1];
			} else if (arg.contains("accesslog")) {
				filename = temp[1];
			}

		}

		if (!"N".equalsIgnoreCase(upload)) {
			service.saveLog(filename);
		}
		service.findLimit(startDate, duration, threshold);

	}

	@PostConstruct
	public void initApplication() {
		service = logService;
	}

}
