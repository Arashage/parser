package com.ef.service;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.ef.domain.Block;
import com.ef.repository.BlockRepository;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ef.domain.LogHistory;
import com.ef.repository.LogHistoryRepository;

@Service
public class LogService {

	@Autowired
	private LogHistoryRepository logHistoryRepository;

	@Autowired
	private BlockRepository blockRepository;

	public long saveLog(String filename) {

		System.out.println("----- Start Upload Log -----");

		long lineCount = 0;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");

		FileInputStream fstream;
		BufferedReader br = null;
		List<LogHistory> accessList = new ArrayList<>();

		try {
			fstream = new FileInputStream(filename);
			br = new BufferedReader(new InputStreamReader(fstream));

			String strLine;

			while ((strLine = br.readLine()) != null) {

				int index = 0;
				String[] lineData = strLine.split("\\|");
				lineCount++;

				if (lineData.length >= 5) {
					LogHistory logHistory = new LogHistory();

					try {
						logHistory.setDate(dateFormat.parse(lineData[index++]));
					} catch (Exception exception) {
						System.out.println("Line number " + lineCount + " has invalid date format");
					}

					logHistory.setIp(lineData[index++]);
					logHistory.setRequest(lineData[index++]);
					logHistory.setStatus(lineData[index++]);
					logHistory.setUserAgent(lineData[index]);
					accessList.add(logHistory);

				} else {
					System.out.println("Line number " + lineCount + " has mismatch number of columns");
				}

			}

			logHistoryRepository.deleteAll();
			logHistoryRepository.saveAll(accessList);

			System.out.println("Upload Done");
			System.out.println("From file " + filename + " found " + lineCount + " records.");

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return accessList.size();

	}

	public long findLimit(String date, String duration, String threshold) {

		List<Block> blockList = new ArrayList<>();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd.hh:mm:ss");

		try {

			Date fromDate = dateFormat.parse(date);
			Calendar cal = Calendar.getInstance();
			cal.setTime(fromDate);

			if (duration.equalsIgnoreCase("hourly")) {
				cal.add(Calendar.HOUR, 1);
			} else if (duration.equalsIgnoreCase("daily")) {
				cal.add(Calendar.DATE, 1);
			}

			Date toDate = cal.getTime();

			if (NumberUtils.isParsable(threshold)) {

				Long thresholdValue = Long.valueOf(threshold);
				List<String> overLimitIpList = logHistoryRepository.findLimitCount(fromDate, toDate, thresholdValue);

				for (String overLimitIp : overLimitIpList) {
					System.out.println(overLimitIp);

					Block block = new Block();
					block.setIp(overLimitIp);
					block.setReason("  has " + threshold + " or more requests between " + fromDate + " and " + toDate);
					block.setInputDate(new Date());
					blockList.add(block);
				}

				blockRepository.saveAll(blockList);

			} else {
				System.out.println("Threshold should be number.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return blockList.size();

	}

}
