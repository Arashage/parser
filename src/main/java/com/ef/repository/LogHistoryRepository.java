package com.ef.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ef.domain.LogHistory;

public interface LogHistoryRepository extends JpaRepository<LogHistory, Long>, JpaSpecificationExecutor<LogHistory> {

	@Query("SELECT ip " + //
			"FROM LOG_HISTORY " + //
			"WHERE date BETWEEN :fromDate AND :toDate " + //
			"GROUP BY ip " + //
			"HAVING COUNT(ip) >= :treshold ")
	List<String> findLimitCount(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate,
			@Param("treshold") long treshold);

}
