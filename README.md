# LogReader

#[Database]
- MySQL
- Username: root
- Password: root

** Schema and Database will generate auto


#[Pakage] (Build Jar)
```sh
mvn clean package
mvn clean package -Dmaven.test.skip=true (skip test)
```

#[Arguments]
```sh
--startDate
```
you can specific start date range here
default value "2017-01-01.00:00:00"

```sh
--duration
```
can specific between "hourly" and "daily"
default value "hourly"

```sh
--threshold 
```
can specific by number value

```sh
--accesslog
```
file name of log. You can specific file name or absolute path
default value "access.log" (if you not specific path then file must put in same place with jar file)

```sh
--upload
``` 
upload status.
- "Y" mean delete all and upload value from file.
- "N" mean skip upload and use current value in database
- default value "Y"

# Test
```sh
java -cp "parser.jar" com.ef.Parser --startDate=2017-01-01.13:00:00 --duration=hourly --threshold=100

----- Start Upload Log -----
Upload Done
From file access.log found 116484 records.
192.168.228.188
192.168.77.101
```

```sh
java -cp "parser.jar" com.ef.Parser --startDate=2017-01-01.13:00:00 --duration=daily --threshold=250 --upload=N

192.168.203.111
192.168.51.205
192.168.31.26
192.168.199.209
192.168.33.16
192.168.62.176
192.168.143.177
192.168.38.77
192.168.129.191
192.168.52.153
192.168.162.248
192.168.219.10
```

```sh
java -cp "parser.jar" com.ef.Parser --startDate=2017-01-01.15:00:00 --duration=hourly --threshold=200 --upload=N

192.168.106.134
192.168.11.231
```

```sh
java -cp "parser.jar" com.ef.Parser --startDate=2017-01-01.00:00:00 --duration=daily --threshold=500 --accesslog=d:\Git\parser\access.log

----- Start Upload Log -----
Upload Done
From file access.log found 116484 records.
192.168.162.248
192.168.199.209
192.168.102.136
192.168.38.77
192.168.62.176
192.168.203.111
192.168.185.164
192.168.52.153
192.168.129.191
192.168.206.141
192.168.51.205
192.168.143.177
192.168.31.26
192.168.219.10
192.168.33.16
```

# Remark
> If you cannot run check that you have local DB on change configuration and create new war file.

> Also check that your table not lock by liquibase if you stop/interupt process. You can drop schema and rerun.
